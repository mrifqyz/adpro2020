package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.guild = guild;
                this.name = "Agile";
                guild.add(this);
        }

        @Override
        public void update() {
                if(guild.getQuestType().equals("D") || guild.getQuestType().equals("R"))
                        this.getQuests().add(guild.getQuest());
        }
}
