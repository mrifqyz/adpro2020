package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    private String alias;

    public AgileAdventurer(){
        this.alias = "Agile";
        attackBehavior = new AttackWithGun();
        defenseBehavior = new DefendWithBarrier();
    }

    @Override
    public String getAlias() {
        return alias;
    }

}
