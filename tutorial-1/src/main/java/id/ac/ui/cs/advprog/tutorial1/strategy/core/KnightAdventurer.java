package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    private String alias;

    public KnightAdventurer(){
        this.alias = "Knight";
        attackBehavior = new AttackWithSword();
        defenseBehavior = new DefendWithArmor();
    }

    @Override
    public String getAlias() {
        return alias;
    }
}
